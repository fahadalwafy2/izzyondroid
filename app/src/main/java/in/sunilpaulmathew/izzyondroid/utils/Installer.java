package in.sunilpaulmathew.izzyondroid.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.widget.LinearLayoutCompat;

import com.google.android.material.textview.MaterialTextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.activities.InstallerActivity;
import in.sunilpaulmathew.izzyondroid.services.InstallerService;
import in.sunilpaulmathew.sCommon.Utils.sExecutor;
import in.sunilpaulmathew.sCommon.Utils.sInstallerParams;
import in.sunilpaulmathew.sCommon.Utils.sInstallerUtils;
import in.sunilpaulmathew.sCommon.Utils.sUtils;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class Installer {

    private static Intent getCallbackIntent(Context context) {
        return new Intent(context, InstallerService.class);
    }

    public static void installAPK(File APK, Activity activity) {
        new sExecutor() {

            @Override
            public void onPreExecute() {
                sUtils.saveString("installationStatus", "waiting", activity);
                Intent installIntent = new Intent(activity, InstallerActivity.class);
                installIntent.putExtra(InstallerActivity.PATH_INTENT, APK.getAbsolutePath());
                activity.startActivity(installIntent);
            }

            @Override
            public void doInBackground() {
                int sessionId;
                final sInstallerParams installParams = sInstallerUtils.makeInstallParams(APK.length());
                sessionId = sInstallerUtils.runInstallCreate(installParams, activity);
                try {
                    sInstallerUtils.runInstallWrite(APK.length(), sessionId, APK.getName(), APK.getAbsolutePath(), activity);
                } catch (NullPointerException ignored) {}
                sInstallerUtils.doCommitSession(sessionId, getCallbackIntent(activity), activity);
            }

            @Override
            public void onPostExecute() {
            }
        }.execute();
    }

    public static void installPackage(LinearLayoutCompat progress, LinearLayoutCompat buttons, MaterialTextView status,
                                      String url, File path, Activity activity) {
        new sExecutor() {

            @Override
            public void onPreExecute() {
                Common.isDownloading(true);
                status.setText(activity.getString(R.string.downloading, Common.getAppName()));
                buttons.setVisibility(View.GONE);
                progress.setVisibility(View.VISIBLE);
                if (path.exists()) {
                    sUtils.delete(path);
                }
            }

            @Override
            public void doInBackground() {
                try (InputStream input = new URL(url).openStream();
                     OutputStream output = new FileOutputStream(path)) {
                    byte[] data = new byte[4096];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                } catch (Exception ignored) {
                }
            }

            @Override
            public void onPostExecute() {
                if (path.exists()) {
                    Installer.installAPK(path, activity);
                    activity.finish();
                } else {
                    progress.setVisibility(View.GONE);
                    buttons.setVisibility(View.VISIBLE);
                    sUtils.snackBar(activity.findViewById(android.R.id.content), activity.getString(R.string.download_failed)).show();
                }
                Common.isDownloading(false);
            }
        }.execute();
    }

}