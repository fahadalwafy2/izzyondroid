package in.sunilpaulmathew.izzyondroid.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.adapters.SearchingAdapter;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewData;
import in.sunilpaulmathew.sCommon.Utils.sExecutor;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class PackageSearchingFragment extends Fragment {

    private AppCompatEditText mSearchWord;
    private LinearLayoutCompat mProgressLayout;
    private RecyclerView mRecyclerView;
    private static SearchingAdapter mRecycleViewAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_package_search, container, false);

        AppCompatImageButton mBack = mRootView.findViewById(R.id.back);
        mSearchWord = mRootView.findViewById(R.id.search_word);
        mProgressLayout = mRootView.findViewById(R.id.progress_layout);
        mRecyclerView = mRootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(requireActivity(), 1));

        loadUI(mProgressLayout, mRecyclerView);

        mSearchWord.requestFocus();

        mSearchWord.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Common.setSearchText(s.toString().toLowerCase());
                loadUI(mProgressLayout, mRecyclerView);
            }
        });

        mBack.setOnClickListener( v-> {
            Common.setSearchText(null);
            requireActivity().finish();
        });

        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (Common.getSearchText() != null && !Common.getSearchText().isEmpty()) {
                    Common.setSearchText(null);
                    mSearchWord.setText(null);
                    loadUI(mProgressLayout, mRecyclerView);
                } else {
                    requireActivity().finish();
                }
            }
        });

        return mRootView;
    }

    private static void loadUI(LinearLayoutCompat linearLayout, RecyclerView recyclerView) {
        new sExecutor() {

            @Override
            public void onPreExecute() {
                linearLayout.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }

            @Override
            public void doInBackground() {
                mRecycleViewAdapter = new SearchingAdapter(RecyclerViewData.getSearchData());
            }

            @Override
            public void onPostExecute() {
                recyclerView.setAdapter(mRecycleViewAdapter);
                linearLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }
        }.execute();
    }

}