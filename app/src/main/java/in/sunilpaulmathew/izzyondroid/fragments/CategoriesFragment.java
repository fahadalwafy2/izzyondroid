package in.sunilpaulmathew.izzyondroid.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewData;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class CategoriesFragment extends Fragment {

    private AppCompatEditText mSearchWord;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mRootView = inflater.inflate(R.layout.fragment_categories, container, false);

        AppCompatImageButton mBack = mRootView.findViewById(R.id.back);
        AppCompatImageButton mSearch = mRootView.findViewById(R.id.search);
        mSearchWord = mRootView.findViewById(R.id.search_word);
        MaterialTextView mTitle = mRootView.findViewById(R.id.title);
        LinearLayoutCompat mProgressLayout = mRootView.findViewById(R.id.progress_layout);
        RecyclerView mRecyclerView = mRootView.findViewById(R.id.recycler_view);

        mTitle.setText(RecyclerViewData.getCategories(requireActivity()).get(Common.getTabPosition()).getTitle());

        mRecyclerView.setLayoutManager(new GridLayoutManager(requireActivity(), RecyclerViewData.getSpanCount(6, 3, requireActivity())));
        RecyclerViewData.loadCategories(RecyclerViewData.getCategories(requireActivity()).get(Common.getTabPosition()).getTitle(), mProgressLayout, mRecyclerView, requireActivity());

        mSearch.setOnClickListener(v -> {
            if (mSearchWord.getVisibility() == View.VISIBLE) {
                mSearchWord.setVisibility(View.GONE);
                mTitle.setVisibility(View.VISIBLE);
                toggleKeyboard(0);
            } else {
                mSearchWord.setVisibility(View.VISIBLE);
                mTitle.setVisibility(View.GONE);
                toggleKeyboard(1);
            }
        });

        mSearchWord.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Common.setSearchText(s.toString().toLowerCase());
                RecyclerViewData.loadCategories(RecyclerViewData.getCategories(requireActivity()).get(Common.getTabPosition()).getTitle(), mProgressLayout, mRecyclerView, requireActivity());
            }
        });

        mBack.setOnClickListener( v-> {
            Common.setSearchText(null);
            requireActivity().finish();
        });

        requireActivity().getOnBackPressedDispatcher().addCallback(new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (Common.getSearchText() != null && !Common.getSearchText().isEmpty()) {
                    Common.setSearchText(null);
                    mSearchWord.setText(null);
                    RecyclerViewData.loadCategories(RecyclerViewData.getCategories(requireActivity()).get(Common.getTabPosition()).getTitle(), mProgressLayout, mRecyclerView, requireActivity());
                } if (mSearchWord.getVisibility() == View.VISIBLE) {
                    mSearchWord.setVisibility(View.GONE);
                    mTitle.setVisibility(View.VISIBLE);
                } else {
                    requireActivity().finish();
                }
            }
        });

        return mRootView;
    }

    private void toggleKeyboard(int mode) {
        InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (mode == 1) {
            if (mSearchWord.requestFocus()) {
                imm.showSoftInput(mSearchWord, InputMethodManager.SHOW_IMPLICIT);
            }
        } else {
            imm.hideSoftInputFromWindow(mSearchWord.getWindowToken(), 0);
        }
    }

}