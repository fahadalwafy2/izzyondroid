package in.sunilpaulmathew.izzyondroid.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.fragments.CategoriesFragment;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on September 04, 2021
 */
public class CategoriesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_fragment);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new CategoriesFragment()).commit();
    }

}