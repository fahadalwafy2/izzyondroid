package in.sunilpaulmathew.izzyondroid.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewData;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on September 02, 2021
 */
public class DeveloperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);

        AppCompatImageButton mBack = findViewById(R.id.back);
        LinearLayoutCompat mProgressLayout = findViewById(R.id.progress_layout);
        MaterialTextView mTitle = findViewById(R.id.title);
        RecyclerView mRecyclerView = findViewById(R.id.recycler_view);

        mTitle.setText(getString(R.string.apps_by_developer, Common.getAuthorName()));

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, RecyclerViewData.getSpanCount(6, 3, this)));
        RecyclerViewData.loadDeveloperApps(mProgressLayout, mRecyclerView, this);

        mBack.setOnClickListener(v -> finish());
    }

}