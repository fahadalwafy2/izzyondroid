package in.sunilpaulmathew.izzyondroid.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textview.MaterialTextView;
import com.squareup.picasso.Picasso;

import java.util.List;

import in.sunilpaulmathew.izzyondroid.R;
import in.sunilpaulmathew.izzyondroid.utils.Common;
import in.sunilpaulmathew.izzyondroid.utils.RecyclerViewItems;

/*
 * Created by sunilpaulmathew <sunil.kde@gmail.com> on August 19, 2021
 */
public class SearchingAdapter extends RecyclerView.Adapter<SearchingAdapter.ViewHolder> {

    private static List<RecyclerViewItems> data;
    public SearchingAdapter(List<RecyclerViewItems> data){
        SearchingAdapter.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_searching, parent, false);
        return new ViewHolder(rowItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.mName.setText(data.get(position).getTitle());
            if (data.get(position).getSummary() != null) {
                holder.mSummary.setText(data.get(position).getSummary());
            } else {
                holder.mSummary.setVisibility(View.GONE);
            }
            Picasso.get().load(data.get(position).getImageUrl()).placeholder(R.drawable.ic_android).into(holder.mIcon);
        } catch (IndexOutOfBoundsException | NullPointerException ignored) {}
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AppCompatImageView mIcon;
        private final MaterialTextView mName, mSummary;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.mIcon = view.findViewById(R.id.icon);
            this.mSummary = view.findViewById(R.id.description);
            this.mName = view.findViewById(R.id.title);
        }

        @Override
        public void onClick(View view) {
            Common.launchPackageView(data.get(getAdapterPosition()), view.getContext());
        }
    }

}